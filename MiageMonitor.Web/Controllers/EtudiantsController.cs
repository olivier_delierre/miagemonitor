﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MiageMonitor.Data.Contexts;
using MiageMonitor.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace MiageMonitor.Web.Controllers
{
    public class EtudiantsController : Controller
    {
        private readonly MiageContext _miageContext;
        private readonly HttpClient _httpClient;

        public EtudiantsController(MiageContext miageContext)
        {
            _miageContext = miageContext;
            _httpClient = new HttpClient();
        }


        [HttpGet]
        public IEnumerable<Etudiant> GetEtudiantsFromEntityFramework()
        {
            IEnumerable<Etudiant> etudiants = null;
            try
            {
                etudiants = _miageContext.Etudiants;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return etudiants;
        }

        [HttpGet]
        public IEnumerable<Etudiant> GetEtudiantsFromLocalAPI()
        {
            IEnumerable<Etudiant> etudiants = null;
            try
            {
                string json = _httpClient.GetStringAsync("https://localhost:5003/api/Etudiants").Result;
                etudiants = JsonConvert.DeserializeObject<IEnumerable<Etudiant>>(json);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return etudiants;
        }

        [HttpPost]
        public IActionResult AddEtudiantFromLocalAPI(Etudiant etudiant)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            
            var result = _httpClient.PostAsJsonAsync("https://localhost:5003/api/Etudiants", etudiant).Result;

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult AddEtudiantFromLocalAPI()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddEtudiantFromEntityFramework(Etudiant etudiant)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            _miageContext.Etudiants.Add(etudiant);
            _miageContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult AddEtudiantFromEntityFramework()
        {
            return View();
        }

        public IActionResult Index()
        {
            ViewBag.EtudiantsFromEF = GetEtudiantsFromEntityFramework();
            ViewBag.EtudiantsFromLocalAPI = GetEtudiantsFromLocalAPI();

            return View();
        }
    }
}