﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MiageMonitor.Data.Contexts;
using MiageMonitor.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MiageMonitor.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EtudiantsController : ControllerBase
    {
        private readonly MiageContext _miageContext;

        public EtudiantsController(MiageContext miageContext)
        {
            _miageContext = miageContext;
        }

        [HttpGet]
        public ActionResult<List<Etudiant>> Get()
        {
            return _miageContext.Etudiants.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Etudiant> Get(int id)
        {
            return _miageContext.Etudiants.SingleOrDefault(x => x.EtudiantId == id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Etudiant etudiant)
        {
            _miageContext.Etudiants.Add(etudiant);

            _miageContext.SaveChanges();
        }

        [HttpPut]
        public void Put(int id, [FromBody] Etudiant etudiant)
        {
            var previousEtudiant = _miageContext.Etudiants.SingleOrDefault(x => x.EtudiantId == id);

            if (previousEtudiant != null)
            {
                int previousId = previousEtudiant.EtudiantId;
                previousEtudiant = etudiant;
                etudiant.EtudiantId = id;
                _miageContext.SaveChanges();
            }
        }

        [HttpDelete]
        public void Delete([FromBody] int id)
        {
            var etudiant = _miageContext.Etudiants.SingleOrDefault(x => x.EtudiantId == id);

            if (etudiant != null)
            {
                _miageContext.Etudiants.Remove(etudiant);
                _miageContext.SaveChanges();
            }
        }
    }
}