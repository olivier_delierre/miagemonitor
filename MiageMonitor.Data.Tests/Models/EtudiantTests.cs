﻿using MiageMonitor.Data.Models;
using System;
using Xunit;

namespace MiageMonitor.Data.Tests.Models
{
    public class EtudiantTests
    {
        [Theory]
        [InlineData(1111111)]
        [InlineData(111111111)]
        public void Etudiant_HasNumeroEtudiantCorrect_Invalid(int numeroEtudiant)
        {
            Etudiant etudiant = new Etudiant { NumeroEtudiant = numeroEtudiant };
            Assert.False(etudiant.HasNumeroEtudiantCorrect());
        }

        [Fact]
        public void Etudiant_HasNumeroEtudiantCorrect_Valid()
        {
            Etudiant etudiant = new Etudiant { NumeroEtudiant = 11111111 };
            Assert.True(etudiant.HasNumeroEtudiantCorrect());
        }

        [Theory]
        [InlineData(18)]
        [InlineData(19)]
        public void Etudiant_IsAdult_Valid(int age)
        {
            Etudiant etudiant = new Etudiant { DateNaissance = new DateTime(
                DateTime.Now.Year - age,
                DateTime.Now.Month,
                DateTime.Now.Day) };
            Assert.True(etudiant.IsAdulte());
        }

        [Fact]
        public void Etudiant_IsAdult_Invalid()
        {
            Etudiant etudiant = new Etudiant { DateNaissance = new DateTime(
                DateTime.Now.Year - 17,
                DateTime.Now.Month,
                DateTime.Now.Day)};
            Assert.False(etudiant.IsAdulte());
        }
    }
}
