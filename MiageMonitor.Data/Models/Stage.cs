﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiageMonitor.Data.Models
{
    public class Stage
    {
        public int StageId { get; set; }

        public string NomEntreprise { get; set; }
        public int DureeMois { get; set; }
        public string Description { get; set; }
    }
}
