﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MiageMonitor.Data.Models
{
    public class Etudiant
    {
        [DisplayName("Identifiant")]
        public int EtudiantId { get; set; }

        [Required(ErrorMessage = "Le champs {0} est obligatoire.")]
        [DisplayName("Nom")]
        [StringLength(20, ErrorMessage = "Le champs {0} doit posséder entre {2} et {1} caractères.", MinimumLength = 3)]
        public string Nom { get; set; }

        [Required(ErrorMessage = "Le champs {0} est obligatoire.")]
        [DisplayName("Prénom")]
        [StringLength(20, ErrorMessage = "Le champs {0} doit posséder entre {2} et {1} caractères.", MinimumLength = 3)]
        public string Prenom { get; set; }

        [Required(ErrorMessage = "Le champs {0} est obligatoire.")]
        [DisplayName("Date de naissance")]
        public DateTime DateNaissance { get; set; }

        [Required(ErrorMessage = "Le champs {0} est obligatoire.")]
        [DisplayName("Numéro étudiant")]
        public int NumeroEtudiant { get; set; }

        public List<Stage> Stages { get; set; }

        public bool HasNumeroEtudiantCorrect()
        {
            return NumeroEtudiant.ToString().Length == 8;
        }

        public bool IsAdulte()
        {
            return DateTime.Now.Year - DateNaissance.Year >= 18;
        }
    }
}
