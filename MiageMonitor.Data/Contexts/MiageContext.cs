﻿using MiageMonitor.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiageMonitor.Data.Contexts
{
    public class MiageContext : DbContext
    {
        public DbSet<Etudiant> Etudiants { get; set; }
        DbSet<Stage> Stages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=../data/miage.db");
    }
}
